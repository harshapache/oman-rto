/*
**____________---Finger IO---_____________
**
*/
(function($window){
	console.log("Finger scanner app running");
	var authenticated = false;
	var authCounter = 2;
	var $document = $window.document;
	var board = $document.getElementById('finger-scan');
	var finger = board.getElementsByTagName('svg')[0];
	var paths = finger.getElementsByTagName('path');
	var $loader = $('.loader');
	var $approved = $('.approved');
	var fingerio = {
		'fillSequence':[2,3,4,5,6,7,8,0,1,9],
		'authentication':function(fingerInputs){
			return authCounter--;
		},
		'fillStroke':function(el,color){
			el.setAttribute('stroke',color);
		},
		'refreshStroke':function(paths,color){
			var length = paths.length;
			for(var k=0;k<length;k++){
				fingerio.fillStroke(paths[k],color);
			}
		},
		'fillTimer':function(paths,done){
			var i = paths.length-1;
			var fs = fingerio.fillSequence;
			fingerio.refreshStroke(paths,'#E3E3E3');

			interval = setInterval(function(){
				fingerio.fillStroke(paths[fs[i]],'#FF475E');
				if(i == 0){
					clearInterval(interval);
					if(typeof done === 'function'){
						done();
					}
				}
				i--;
			},1000);
		}
	};

	/*
	** Key start of finger io app ** 
	*/
	
	(function init(){
		function startAuthenticate(){
			$loader.css({'display':'block'});
			fingerio.authentication();
			fingerio.fillTimer(paths,function(){
				console.log('first phase completed');
				if(authCounter === 0 ){
					authenticated = true;
					$loader.css('display','none');
					$approved.css('display','block');
					setTimeout(function(){
						sendToPath('detail.html');
					},1000);
				}
				else{
					startAuthenticate();
				}
			});
		}
		startAuthenticate();
	})();
})(window);