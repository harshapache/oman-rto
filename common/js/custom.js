(function($w){
	/*
	** Common Global Function **
	*/
	$w.sendToPath = function(path){
		var current_path = $w.location.href;
		var path_array = current_path.split('/');
		var l = path_array.length;
		path_array[l-1] = path; 
		$w.location.href = path_array.join('/');
	};
})(window);


jQuery(document).ready(function(){
	/*
	** DOM Ready Function **
	*/
	$('#new-scan').click(function(){
		sendToPath('fingureScan.html')
	})
	$('#new-dl-btn').on('click',function(){ 
		if($(this).hasClass('disabled')){
			return false
			};
		 $('.box-col3').show();
		  $(this).addClass('disabled')
	})

	$("input[name='payment']").on('change',function(){
		$('#payment-done-btn').removeClass('hidden');
	})
	$("#payment-done-btn").on('click',function(){
		$('.box-col4').show();
	})

	
	
});

