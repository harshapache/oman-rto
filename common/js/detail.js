jQuery(document).ready(function(){
		$('#schedule-calender-picker').datetimepicker({
			format:'d m Y H:i'
		});
		$('#schedule-calender-picker-slop').datetimepicker({
			format:'d m Y H:i'
		});

		$('#open-calendar').click(function(){
			$('#schedule-calender-picker').focus();
		});

		$('#open-slope').click(function(){
			$('#schedule-calender-picker-slop').focus();
		});
});